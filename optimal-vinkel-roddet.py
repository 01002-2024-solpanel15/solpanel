import sympy as sym
import numpy as np
from pvlib.location import Location
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from scipy import integrate

loc = Location(55.786327, 12.524295, tz="Europe/Copenhagen", altitude=40, name="DTU Bygning 101")

def times_of_days(start_date, end_date, freq="Min", tz="Europe/Copenhagen"):
    return pd.date_range(start_date + " 00:00:00", end_date + " 23:59:59", inclusive="left", freq=freq, tz=tz)

def solar_positions_of_day(loc, start_date, end_date, freq="Min", tz="Europe/Copenhagen"):
    return loc.get_solarposition(times_of_days(start_date, end_date, freq=freq, tz=tz))

# print(solar_positions_of_day(loc, "2024-01-01", "2024-01-01"))

def solar_panel_projection(theta_sun, phi_sun, theta_panel, phi_panel):
    return np.maximum(0, np.sin(theta_sun) * np.sin(theta_panel) * np.cos(phi_sun - phi_panel) + np.cos(theta_sun) * np.cos(theta_panel))

solpos = solar_positions_of_day(loc, "2024-04-20", "2024-04-20")
theta_sol = np.deg2rad(solpos["zenith"])
phi_sol = np.deg2rad(solpos["azimuth"])
#proj = solar_panel_projection(theta_sol, phi_sol, np.deg2rad(38), np.pi)

#fig, ax = plt.subplots(figsize=(5, 2.7), layout='constrained')
#ax.plot(proj)
#ax.set_ylabel("Projection")
#ax.set_xlabel("Time (hour)")
#ax.xaxis.set_major_formatter(mdates.DateFormatter("%H"))

def flux(theta_sun, phi_sun, theta_panel, phi_panel, a_0=0.5, irradiance=1100):
    proj = solar_panel_projection(theta_sun, phi_sun, theta_panel, phi_panel)
    flux = proj * a_0 * irradiance
    flux = np.where((0 <= theta_sun) & (theta_sun <= np.pi/2), flux, 0)
    return flux

proj = solar_panel_projection(theta_sol, phi_sol, np.deg2rad(0), np.pi)

#f = flux(theta_sol, phi_sol, np.deg2rad(0), np.pi)
#print(f)
#print(np.min(f), np.max(f))
#fig, bx = plt.subplots(figsize=(5, 2.7), layout='constrained')
#bx.plot(f)
#bx.set_ylabel("flux")
#bx.set_xlabel("Time (hour)")
#bx.xaxis.set_major_formatter(mdates.DateFormatter("%H"))

def mange_vinkler(panel_efficiency=0.2148):
    #flux_at_angles = [(theta_panel, integrate.simps(flux(theta_sol, phi_sol, theta_panel, np.pi), dx=60)) * panel_efficiency for theta_panel in range(91)]
    for theta_panel in range(91):
        f = flux(theta_sol, phi_sol, np.deg2rad(theta_panel), np.pi)
        #print(theta_panel, integrate.simps(f, dx=60) * panel_efficiency)

def panel_production_for_year_for_angle(theta_panel, year=2024, panel_efficiency=0.2148, phi_panel=180):
    start_date = str(year) + "-01-01"
    end_date = str(year) + "-12-31"
    solpos = solar_positions_of_day(loc, start_date, end_date)
    theta_sol = np.deg2rad(solpos["zenith"])
    phi_sol = np.deg2rad(solpos["azimuth"])
    f = flux(theta_sol, phi_sol, np.deg2rad(theta_panel), np.deg2rad(phi_panel))
    return integrate.simps(f, dx=60) * panel_efficiency


def panel_production_for_year_for_integer_angles(panel_efficiency=0.2148, year=2024, phi_panel=180):
    start_date = str(year) + "-01-01"
    end_date = str(year) + "-12-31"
    solpos = solar_positions_of_day(loc, start_date, end_date)
    theta_sol = np.deg2rad(solpos["zenith"])
    phi_sol = np.deg2rad(solpos["azimuth"])
    #f = flux(theta_sol, phi_sol, np.deg2rad(theta_panel), np.pi)
    l = []
    for theta_panel in range(91):
        f = flux(theta_sol, phi_sol, np.deg2rad(theta_panel), np.deg2rad(phi_panel))
        l.append((theta_panel, integrate.simps(f, dx=60) * panel_efficiency))
    return l


#print(panel_production_for_year_for_integer_angles())
#fig, cx = plt.subplots(figsize=(5, 2.7), layout='constrained')
#cx.plot(panel_production_for_year())
#cx.set_ylabel("flux in a year")
#cx.set_xlabel("angle")

def optimal_integer_angle_for_year(panel_efficiency=0.2148, year=2024):
    for phi_panel in range(175, 181):
        prod = panel_production_for_year_for_integer_angles(panel_efficiency=panel_efficiency, year=year, phi_panel=phi_panel)
        max_prod = max(prod, key=lambda t: t[1])
        print(phi_panel)
        print(max_prod)

print(panel_production_for_year_for_angle(51) * 60)




