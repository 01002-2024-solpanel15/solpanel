import numpy as np
import sympy as sym
import matplotlib.pyplot as plt
import math
figurstr = (15, 7)

t = np.linspace(0, 2 * np.pi, 1000)
f = np.cos(t)

def find_sign_change_idxs(f):
    return [i for i, (a, b) in enumerate(zip(f[:], f[1:])) if a*b < 0]

def find_sign_change(t, f):
    return [np.average(t[[i, i+1]]) for i in find_sign_change_idxs(f)]

print(find_sign_change(t, f))
