import numpy as np
from pvlib.location import Location
import pandas as pd
from scipy import integrate

frobrugsdagsforedeling = pd.read_csv("DagsTimeFordeling.csv", index_col="HourOfDay", decimal=",", sep=";")

months = range(1,13)
summermonths = list(range(4,10))
wintermonths = [month for month in months if month not in summermonths]

def summer_or_winter(date):
    month = date.split("-")[1].removeprefix("0")
    return "summer" if int(month) in summermonths else "winter"

def hour_from_date(date):
    return date.split(" ")[1].split(":")[0]

def summer_or_winter_and_hour(date):
    return f"{summer_or_winter(date)}-{hour_from_date(date)}"

percentagesOfDay = pd.read_csv("PercentagesOfDay.csv", delimiter=";", decimal=",", index_col="HourDK", parse_dates=["HourDK"])

dagligForbrug = 4500/365

timeForbrug=(percentagesOfDay*dagligForbrug).rename(columns={ "PercentageOfDay": "Forbrug" })

def askLocation():
    print("indtast breddegrad")
    bGrad = float(input())
    print("indtast længdegrad")
    lGrad = float(input())
    print("indtast højde over havoverfladen")
    alt = float(input())
    return Location(bGrad, lGrad, tz="Europe/Copenhagen", altitude=alt)

def getPanelPhiFromLoc(loc):
    return 180 if loc.latitude > 0 else 0

def times_of_days(start_date, end_date, freq="Min", tz="Europe/Copenhagen"):
    return pd.date_range(start_date + " 00:00:00", end_date + " 23:59:59", inclusive="left", freq=freq, tz=tz)

def solar_positions_of_day(loc, start_date, end_date, **kwargs):
    return loc.get_solarposition(times_of_days(start_date, end_date, **kwargs))

loc = askLocation()

sunpos = solar_positions_of_day(loc, "2024-01-01", "2024-12-31", tz=None)

def solar_panel_projection(theta_sun, phi_sun, theta_panel, phi_panel):
    return np.maximum(0, np.sin(theta_sun) * np.sin(theta_panel) * np.cos(phi_sun - phi_panel) + np.cos(theta_sun) * np.cos(theta_panel))

sun_zeniths = np.deg2rad(sunpos["zenith"])
sun_azimuths = np.deg2rad(sunpos["azimuth"])

def flux(sunpos, theta_panel, phi_panel, a_0=0.5, irradiance=1100):
    theta_sun = np.deg2rad(sunpos["zenith"])
    phi_sun = np.deg2rad(sunpos["azimuth"])
    proj = solar_panel_projection(theta_sun, phi_sun, theta_panel, phi_panel)
    flux = proj * a_0 * irradiance
    flux = np.where((0 <= theta_sun) & (theta_sun <= np.pi/2), flux, 0)
    return flux

minutForbrug=timeForbrug.reindex(sunpos.index, method="pad")/60

def panel_production_for_year_for_integer_angles(loc, phi_panel=getPanelPhiFromLoc(loc), panel_efficiency=0.2148, year=2024):
    start_date = str(year) + "-01-01"
    end_date = str(year) + "-12-31"
    sol_pos = solar_positions_of_day(loc, start_date, end_date, tz=None)
    theta_sol = np.deg2rad(sol_pos["zenith"])
    phi_sol = np.deg2rad(sol_pos["azimuth"])
    l = []
    for theta_panel in range(91):
        f = flux(sol_pos, np.deg2rad(theta_panel), np.deg2rad(phi_panel))
        l.append((theta_panel, integrate.simpson(f, dx=60) * panel_efficiency / 3600000))
    return l

def year_net_import_minutes(sol_pos, theta_panel, phi_panel, areal, panel_efficiency=0.2148):
    theta_sol = np.deg2rad(sol_pos["zenith"])
    phi_sol = np.deg2rad(sol_pos["azimuth"])
    d =  minutForbrug.assign(Produktion=flux(sunpos, theta_panel, phi_panel)*areal*panel_efficiency/3600000*60)
    return d["Forbrug"] - d["Produktion"]/60

hourBuyPrices = pd.read_csv("consumer-prices-2020.csv", sep=";", decimal=",", index_col="HourDK", parse_dates=["HourDK"]).drop(columns=["Unnamed: 0"])

hourSellPrices = pd.read_csv("Elspotprices-2020.csv", sep=";", decimal=",", index_col="HourDK", parse_dates=["HourDK"], usecols=["HourDK", "SpotPriceDKK"])

minutePrices = hourBuyPrices.join(hourSellPrices).reindex(sunpos.index, method="pad")

def year_money(sol_pos, theta_panel, phi_panel, areal):
    net_import_minutes = year_net_import_minutes(sol_pos, theta_panel, phi_panel, areal)
    net_import_minutes[net_import_minutes < 0] *= minutePrices["SpotPriceDKK"]/100
    net_import_minutes[net_import_minutes > 0] *= minutePrices["ConsumerPriceDKK"]/100
    return integrate.simpson(net_import_minutes)

def optimise_angle_according_to_price(loc, area, phi_panel=getPanelPhiFromLoc(loc), panel_efficiency=0.2148):
    year = 2020
    start_date = str(year) + "-01-01"
    end_date = str(year) + "-12-31"
    sol_pos = solar_positions_of_day(loc, start_date, end_date, tz=None)
    l = []
    for theta_panel in range(91):
        l.append((theta_panel, year_money(sol_pos, np.deg2rad(theta_panel), np.deg2rad(phi_panel), area)))
    return l

print(optimise_angle_according_to_price(loc, 60))