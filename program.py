import numpy as np
from pvlib.location import Location
import pandas as pd
from scipy import integrate

def getLocation():
    print("indtast breddegrad")
    bGrad = float(input())
    print("indtast længdegrad")
    lGrad = float(input())
    print("indtast altitude")
    altitude = float(input())
    return bGrad, lGrad, altitude

bGrad, lGrad, altitude = getLocation()

loc = Location(bGrad, lGrad, tz="Europe/Copenhagen", altitude=altitude, name="DTU Bygning 101")


def setPhiPanel(bGrad):
    phi_panel = 0
    if bGrad > 0:
        phi_panel = 180
    else:
        phi_panel = 0
    return phi_panel

phi_panel = setPhiPanel(bGrad)

def times_of_days(start_date, end_date, freq="Min", tz="Europe/Copenhagen"):
    return pd.date_range(start_date + " 00:00:00", end_date + " 23:59:59", inclusive="left", freq=freq, tz=tz)

def solar_positions_of_day(loc, start_date, end_date, freq="Min", tz="Europe/Copenhagen"):
    return loc.get_solarposition(times_of_days(start_date, end_date, freq=freq, tz=tz))

def solar_panel_projection(theta_sun, phi_sun, theta_panel, phi_panel):
    return np.maximum(0, np.sin(theta_sun) * np.sin(theta_panel) * np.cos(phi_sun - phi_panel) + np.cos(theta_sun) * np.cos(theta_panel))

solpos = solar_positions_of_day(loc, "2024-01-01", "2024-12-31")
theta_sol = np.deg2rad(solpos["zenith"])
phi_sol = np.deg2rad(solpos["azimuth"])

def flux(theta_sun, phi_sun, theta_panel, phi_panel, a_0=0.5, irradiance=1100):
    proj = solar_panel_projection(theta_sun, phi_sun, theta_panel, phi_panel)
    flux = proj * a_0 * irradiance
    flux = np.where((0 <= theta_sun) & (theta_sun <= np.pi/2), flux, 0)
    return flux


def panel_production_for_year_for_integer_angles(phi_panel, panel_efficiency=0.2148, year=2024):
    l = []
    for theta_panel in range(91):
        f = flux(theta_sol, phi_sol, np.deg2rad(theta_panel), np.deg2rad(phi_panel))
        l.append((theta_panel, integrate.simps(f, dx=60) * panel_efficiency / 3600000))
    return l

def optimal_integer_angle_for_year(panel_efficiency=0.2148, year=2024):
    prod = panel_production_for_year_for_integer_angles(phi_panel, panel_efficiency=panel_efficiency, year=2024)
    max_prod = max(prod, key=lambda t: t[1])
    print("panelets azimut-vinkel skal være:", phi_panel)
    print("Nedenfor ser du dit panels zenit-vinkel samt hvor mange kWh du vil producere pr. kvadratmeter pr. år ")
    print(max_prod)

optimal_integer_angle_for_year()